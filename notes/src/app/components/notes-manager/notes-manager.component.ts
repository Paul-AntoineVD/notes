import { ThrowStmt } from '@angular/compiler';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Note } from 'src/app/models/Note';
import { formatDate } from '@angular/common';
import { ApiService } from 'src/app/services/api/api.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-notes-manager',
  templateUrl: './notes-manager.component.html',
  styleUrls: ['./notes-manager.component.scss']
})
export class NotesManagerComponent implements OnInit {

  popup = {show: false, title: "", content: ""};

  notes: Note[];
  notes_backup: Note[];

  triList: string[] = ["Aa-Z↓", "Aa-Z↑", "12.3↓", "12.3↑", "Date↓", "Date↑"];
  tri: string = this.triList[0];

  selectedNote: number = -1;
  previousSelectedNote: number = -1;

  toSearch: string = "";

  constructor(private api: ApiService,) { 
    this.notes = [];
    this.notes_backup = [];
    
    this.api.getNotes().subscribe( notes => {
      this.notes = notes;
      this.notes_backup = this.notes;
    })
  }

  ngOnInit(): void {
  }

  addNote() {
    let note = new Note();
    note.init("A title", this.getNewEditionDate(), "Contents...");

    this.api.addNote(note).subscribe( note => {
      this.notes.push(note);

      function scrollTo() {
        let el = document.getElementsByClassName("note-created");
        el[el.length-1].scrollIntoView({behavior: "smooth"})
      }

      timer(60).subscribe(scrollTo);

     });
  }

  removeSelectedNote() {
    if (this.selectedNote == -1) {
      this.popup = {show: true, title: "Delete failed !", content: "Could not delete the note, no note selected.."};
    } else {
      this.removeNote(this.selectedNote);
    }
  }

  removeNote(i: number) {
    this.api.deleteNote(this.notes[i]).subscribe(isDeleted => {
      if (isDeleted) {
        this.notes.splice(i, 1);
        if (this.selectedNote == i)
          this.selectedNote = -1;
        else if (this.selectedNote > i) {
          this.selectedNote -= 1;
        }
      } 
    })
  }

  selectNote(i: number) {
    this.previousSelectedNote = this.selectedNote;
    this.selectedNote = i;
  }

  search() {
    // todo
    if (this.notes == this.notes_backup)
      this.notes_backup = this.notes;

    this.notes = this.notes.filter(n => n.content.includes(this.toSearch) || n.title.includes(this.toSearch) || n.date.includes(this.toSearch));
  }

  checkfieldempty(event: KeyboardEvent) {
    if (this.toSearch.length == 0)
      this.notes = this.notes_backup;

    if (event.key.match("Enter"))
      this.search();
  }

  updateTitle(event: any) {
    let target = event.target as HTMLDivElement;
    this.notes[this.selectedNote].title = target.innerText;
    this.notes[this.selectedNote].date = this.getNewEditionDate();
    this.update(this.selectedNote);
  }

  updateContent(event: any) {
    let target = event.target as HTMLDivElement;
    this.notes[this.selectedNote].content = target.innerText;
    this.notes[this.selectedNote].date = this.getNewEditionDate();
    this.update(this.selectedNote);
  }

  update(index: number) {
    if (this.notes[index].id != -1) {
      this.api.updateNote(this.notes[index]).subscribe(note => {});
    }
  }
  

  getNewEditionDate(): string {
    return formatDate(new Date(),'dd/MM/yyyy : HH:mm', 'fr');
  }

  setTri() {
    let index = this.triList.findIndex(e => this.tri == e);

    if (index == this.triList.length-1)
      index = 0;
    else
      index++;

    this.tri = this.triList[index];

    if (this.tri == this.triList[0]) {
      // Alpha Down
      this.notes = this.notes.sort((a,b) => (a.title < b.title ? -1 : 1));
    } else if (this.tri == this.triList[1]) {
      // Alpha Up
      this.notes = this.notes.sort((a,b) => (a.title < b.title ? 1 : -1));
    } else if (this.tri == this.triList[2]) {
      // Id Down
      this.notes = this.notes.sort((a,b) => (a.id < b.id) ? -1 : 1);
    }else if (this.tri == this.triList[3]) {
      // Id Up
      this.notes = this.notes.sort((a,b) => (a.id < b.id) ? 1 : -1);
    } else if (this.tri == this.triList[4]) {
      // Date Up
      this.notes = this.notes.sort((a,b) => (a.date < b.date) ? -1 : 1);
    }else if (this.tri == this.triList[5]) {
      // Date Down
      this.notes = this.notes.sort((a,b) => (a.date < b.date) ? 1 : -1);
    } 
  }

  closePopup() {
    this.popup = {show: false, title: "", content: ""};
  }
}
