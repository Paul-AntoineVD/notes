export class Note {
    id: number = -1;
    title: string = "";
    date: string = "";
    content: string = "";

    constructor() {
    }

    init(title: string, date: string, content: string) {
        this.title = title;
        this.date = date;
        this.content = content;
    }
}