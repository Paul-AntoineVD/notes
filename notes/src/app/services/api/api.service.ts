import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { endpoint } from 'src/app/endpoint';
import { Observable  } from 'rxjs';
import { Note } from 'src/app/models/Note';
import { getLocaleMonthNames } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders ({
      'Content-Type': 'application/json',
    }),    
  };


  getNotes(): Observable<Note[]> {
    let url = endpoint.url + "/note";
    return this.http.get<Note[]>(url, this.httpOptions);
  }

  updateNote(note: Note): Observable<Note> {
    let url = endpoint.url + "/note";
    return this.http.put<Note>(url, note, this.httpOptions);
  }

  addNote(note: Note): Observable<Note> {
    let url = endpoint.url + "/note";
    return this.http.post<Note>(url, note, this.httpOptions);
  }

  deleteNote(note: Note): Observable<boolean> {
    let url = endpoint.url + "/note/" + note.id;
    return this.http.delete<boolean>(url, this.httpOptions);
  }

  erroHandler(error: any) : Observable<any> {
    console.log(error);

    throw error;
  }
}

