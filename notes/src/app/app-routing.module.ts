import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoteComponent } from './components/note/note.component';
import { NotesManagerComponent } from './components/notes-manager/notes-manager.component';

const routes: Routes = [
  { path: '', redirectTo: '/manager', pathMatch: 'full'},
  { path: 'manager', component: NotesManagerComponent },
  { path: 'note', component: NoteComponent },
  { path: '**', redirectTo: '/manager', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
