package fr.vd.note.models;

public interface Note {
	
	public Integer id = null;
	public String title = "";
	public String date = "";
	public String content = "";
	
	public void setId(Integer id);
	public void setTitle(String title);
	public void setDate(String date);
	public void setContent(String content);

	public Integer getId();
	public String getTitle();
	public String getDate();
	public String getContent();
}
