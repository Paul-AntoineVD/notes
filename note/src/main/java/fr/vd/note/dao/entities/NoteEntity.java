package fr.vd.note.dao.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.vd.note.models.Note;

@Entity()
@Table(name = "notes")
public class NoteEntity implements Note {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "content")
	private String content;
	
	public NoteEntity() {
		
	}
	
	public NoteEntity(String title, String date, String content) {
		super();
		this.title = title;
		this.date = date;
		this.content = content;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public String getDate() {
		return this.date;
	}

	@Override
	public String getContent() {
		return this.content;
	}
	
}
