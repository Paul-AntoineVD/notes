package fr.vd.note.dao.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.vd.note.dao.entities.NoteEntity;

@Repository
public interface NoteRepository extends CrudRepository<NoteEntity, Integer> {
	public List<NoteEntity> findAll();
}
