package fr.vd.note.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.vd.note.dao.entities.NoteEntity;
import fr.vd.note.services.NotesServices;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/")
public class NoteController {

	@Autowired
	NotesServices noteService;
	
	@GetMapping("note") 
	public List<NoteEntity> getNotes(){
		return this.noteService.getNotes();
	}
	
	@PutMapping("note")
	public NoteEntity updateNote(@RequestBody NoteEntity note) {
		return this.noteService.updateNote(note);
	}
	
	@PostMapping("note") 
	public NoteEntity addNote(@RequestBody NoteEntity note) {
		return this.noteService.addNote(note);
	}
	
	@DeleteMapping("note/{id}")
	public boolean deleteNote(@PathVariable String id) {
		return this.noteService.deleteNote(id);
	}
}
