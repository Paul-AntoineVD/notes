package fr.vd.note.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.vd.note.dao.entities.NoteEntity;
import fr.vd.note.dao.repositories.NoteRepository;

@Service
public class NotesServices {

	@Autowired
	private NoteRepository notesRepository;
	
	public List<NoteEntity> getNotes() {
		return notesRepository.findAll();
	}

	public NoteEntity updateNote(NoteEntity note) {
		return notesRepository.save(note);
	}

	public NoteEntity addNote(NoteEntity note) {
		note.setId(null);
		return notesRepository.save(note);
	}

	public boolean deleteNote(String id) {
		notesRepository.deleteById(Integer.valueOf(id));
		return true;
	}	
}
